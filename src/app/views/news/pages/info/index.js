import Vue from 'vue';
import App from './index.vue';


Vue.config.productionTip = false;
Vue.config.devtools = true;

new Vue({
  // router,
  // store,
  render: h => h(App),
}).$mount('#app');
