/* ajax请求
 * @Author: tanglin
 * @Date: 2019-01-05 10:36:47
 * @Last Modified by: tanglin
 * @Last Modified time: 2019-02-26 14:54:06
 */

import axios from 'axios';
import qs from 'qs';

/*
 * ajax请求
 * @param {*} url
 * @param {*} params
 * @param {*} options
 */
export function fetchAsync(url, params, options = {}) {
  return new Promise((resolve, reject) => {
    axios({
      method: options.type || 'GET',
      url,
      params,
      data: options.isFormSubmit ? qs.stringify(options.data) : options.data,
      responseType: options.responseType || 'json',
      headers: {
        Accept: options.Accept || 'application/json',
        'Content-Type': options.contentType || 'application/json; charset=UTF-8',
      },
    })
      .then((res) => {
        const data = res.data || eval(`(${res.request.responseText})`);
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

/*
 * 获取指定url参数
 * @param {*} url url
 * @param {*} paraName  key
 */
export function getParam(url, paraName) {
  const arrObj = url.split('?');
  if (arrObj.length > 1) {
    const arrPara = arrObj[1].split('&');
    let arr;
    for (let i = 0; i < arrPara.length; i++) {
      arr = arrPara[i].split('=');
      if (arr != null && arr[0] === paraName) {
        return arr[1];
      }
    }
    return '';
  }
  return '';
}
