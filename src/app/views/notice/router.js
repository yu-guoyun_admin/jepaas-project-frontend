/**
 * @Author : ZiQin Zhai
 * @Date : 2020/8/31 18:30
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2020/8/31 18:30
 * @Description
 * */
import routerPage from '@/components/template/index';
import createRouter from '@/util/RouterModel';
import noticeDetail from './pages/noticeDetail/index.vue';
import index from './index.vue';

export default [{
  path: '/JE-PLUGIN-NOTICE',
  component: routerPage,
  children: [
    createRouter(
      {
        path: '',
        name: 'index',
        meta: {
        },
        component: index,
      }
    ),
    createRouter({
      path: 'noticeDetail',
      name: 'noticeDetail',
      title: '消息详情',
      meta: {
        noWork: true,
      },
      component: noticeDetail,
    }),
  ],
}];
