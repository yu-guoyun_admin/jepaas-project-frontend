/*
 * @Descripttion:点击登录的最基础的按钮类
 * @Author: 张明尧
 * @Date: 2021-01-25 15:44:22
 * @LastEditTime: 2021-02-02 16:34:43
 */
import { fetchLogin } from '../../actions/login';
import { decipherStrBtoa } from '../../util/oauthLogin';
export default class BasicsLogin {
  constructor ({ vm }) {
    this.vm = vm; // VUE的实例对象
    this.param = {}; // 当前类型的接口参数
    this.init(); // 进入页面初始化登录事件
  }

  init() {
    // TODO 初始化基础登录可能要配置的一些事件
  }

  /**
   * 设置param的参数值
   * @param {Object} loginInfo
   */
  setParam(loginInfo) {
    this.param = {
      j_username: loginInfo.phone,
      j_password: loginInfo.pwd,
      isNew: 1,
    }
  }

  /**
   * 登录点击之前执行的方法
   */
  beforeClick() {
    // TODO 执行任何方法
    return Promise.resolve({success: true});
  }

  /**
   * 登录按钮点击的方法
   * @param {Object} param 接口需要的参数
   */
  async clickBtn() {
    return this.beforeClick().then(befo => {
      mui('.mui-btn').button('reset');
      if(befo.success) {
        return fetchLogin(this.param).then(res => res)
      }
      mui.toast(befo.message && befo.message || '异常处理');
    });
  }

  /**
   * 点击之后的方法
   */
  afterClick({param, remberStatus}) {
    if (param && remberStatus) {
      JE.setLSItem('phone', decipherStrBtoa(param.phone));
      JE.setLSItem('password', decipherStrBtoa(param.pwd));
    } else {
      localStorage.removeItem('phone');
      localStorage.removeItem('password');
    }
  }
}
