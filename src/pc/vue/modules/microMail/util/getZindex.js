// 获取页面 弹窗的z-index层级值
// 目前知道 EXT的 x-window样式  和 elementui的  el-dialog  先做这两种判断  后面有别的再补充

const getMaxZindex = () => {
  let max = 4999;
  // ext 平台窗体的 层级
  const windowDoms = document.getElementsByClassName('x-window');
  for (let i = 0; i < windowDoms.length; i++) {
    const zIndex = Number(windowDoms[i].style.zIndex);
    max = (zIndex > max ? zIndex : max);
  }
  // ext 列表输入框 层级
  const editorDoms = document.getElementsByClassName('x-editor');
  for (let i = 0; i < editorDoms.length; i++) {
    const zIndex = Number(editorDoms[i].style.zIndex);
    max = (zIndex > max ? zIndex : max);
  }
  // element dialog的 层级
  const dialogwrapperDoms = document.getElementsByClassName('el-dialog__wrapper');
  for (let i = 0; i < dialogwrapperDoms.length; i++) {
    const zIndex = Number(dialogwrapperDoms[i].style.zIndex);
    max = (zIndex > max ? zIndex : max);
  }
  // ueditor  层级
  const ueditorDoms = document.getElementsByClassName('edui-editor');
  for (let i = 0; i < ueditorDoms.length; i++) {
    const zIndex = Number(ueditorDoms[i].style.zIndex);
    max = (zIndex > max ? zIndex : max);
  }
  return max;
};

export default getMaxZindex;
