const m = {
  more: 'More', // 更多
  suredelete: 'Are you sure you want to delete it', // 确定删除吗
  create: 'Create', // 创建
  today: 'Today', // 今天
  retract: 'Collapse', // 收起
  download: 'Download', // 下载
  confirm: 'Confirm', // 确定
  cancel: 'Cancel', // 取消
  remind: 'Remind', // 提醒
  save: 'Save', // 保存
  selectreviewers: 'Select reviewers', // 选择点评人
  close: 'Close', // 关闭
  close1: 'Close', // 关 闭
};

export default m;
