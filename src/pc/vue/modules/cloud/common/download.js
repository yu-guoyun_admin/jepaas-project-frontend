/*
 * author 翟梓钦
 * 用于ajax导出数据
 * @param res
 */
export function download(res) {
  return new Promise((resolve, reject) => {
    try {
      const reg = /filename="\S+"/;
      let fileName = res.responseHeader['content-disposition'].match(reg)[0];
      fileName = decodeURI(fileName.match(/"\S*"/)[0].slice(1, -1));
      // eslint-disable-next-line no-use-before-define
      fileDownload(res.responseText, fileName, res.responseHeader['content-type']);
    } catch (e) {
      reject(e);
    }
  });
}

export function fileDownload(data, filename, mime) {
  const blob = new Blob([data], { type: mime || 'application/octet-stream' });
  if (typeof window.navigator.msSaveBlob !== 'undefined') {
    // IE workaround for "HTML7007: One or more blob URLs were
    // revoked by closing the blob for which they were created.
    // These URLs will no longer resolve as the data backing
    // the URL has been freed."
    window.navigator.msSaveBlob(blob, filename);
  } else {
    const blobURL = URL.createObjectURL(blob);
    const tempLink = document.createElement('a');
    tempLink.style.display = 'none';
    tempLink.href = blobURL;
    tempLink.setAttribute('download', filename);

    // Safari thinks _blank anchor are pop ups. We only want to set _blank
    // target if the browser does not support the HTML5 download attribute.
    // This allows you to download files in desktop safari if pop up blocking
    // is enabled.
    if (typeof tempLink.download === 'undefined') {
      tempLink.setAttribute('target', '_blank');
    }
    document.body.appendChild(tempLink);
    tempLink.click();
    document.body.removeChild(tempLink);
    // 延时释放资源
    setTimeout(() => {
      window.URL.revokeObjectURL(blobURL);
    }, 1000);
  }
}
