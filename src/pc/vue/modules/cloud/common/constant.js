/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-11 10:59:43
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-12-06 16:25:18
 */
import createEnum from './Enum';

export const checkTag = [
  {
    color: '#F6615D',
    check: false,
  },
  {
    color: '#F8A739',
    check: false,
  },
  {
    color: '#F5D03C',
    check: false,
  },
  {
    color: '#69CE49',
    check: false,
  },
  {
    color: '#44B7F5',
    check: false,
  },
  {
    color: '#D185E3',
    check: false,
  },
  {
    color: '#A5A5A8',
    check: false,
  },
];

// 图片类型
export const IMG_FILE = createEnum({
  JPG: 'JPG',
  PNG: 'PNG',
  GIF: 'GIF',
  SVG: 'SVG',
  JEPG: 'JEPG',
  ICO: 'ICO',
  BMP: 'BMP',
});


// 文件类型
export const DOC_FILE = createEnum({
  WORD: 'WORD',
  EXCEL: 'EXCEL',
  PPT: 'PPT',
  PDF: 'PDF',
});

// 视频
export const VIDEO_FILE = createEnum({
  MV: 'MV',
  FLAH: 'FLAH',
});

// 压缩
export const COMPRESS_FILE = createEnum({
  ZIP: 'ZIP',
  RAR: 'RAR',
  CAB: 'CAB',
  TAR: 'TAR',
});

// 音频
export const AUDIO_FILE = createEnum({
  MP3: 'MP3',
  MP4: 'MP4',
});

// 列表类型
export const LIST_TYPE = createEnum({
  DISK_TYPE: 'DISK_TYPE',
  RRCEIVE: 'RRCEIVE',
  RECORD: 'RECORD',
  RECOVERY: 'RECOVERY',
});


// 左侧菜单栏的显示
export const MENU_LIST = [
  {
    title: '网盘',
    icon: 'jeicon-cloud-o',
    router: '1',
    children: [
      {
        title: '个人文件',
        router: '1-2',

      },
      {
        title: '公司文件',
        router: '1-1',
      },
      {
        title: '我发出的分享',
        router: '1-3',
      },
      {
        title: '我收到的分享',
        router: '1-4',
      },
    ],
  },
  {
    title: '正在上传',
    router: '2',
    icon: 'jeicon-upload',
    children: null,
  },
  {
    title: '传输记录',
    router: '3',
    icon: 'jeicon-pass',
    children: null,
  },
  {
    title: '标签',
    router: '4',
    icon: 'jeicon-label-o',
    children: [
      {
        title: '全部',
        icon: 'jeicon-more-round',
        router: '4-1',
        backgroundStatus: true,
      },
      {
        title: '红色',
        color: '#F6615D',
        router: '4-2',
        backgroundStatus: false,
      },
      {
        title: '橙色',
        color: '#F8A646',
        router: '4-3',
        backgroundStatus: false,
      },
      {
        title: '黄色',
        color: '#F4CE4B',
        router: '4-4',
        backgroundStatus: false,
      },
      {
        title: '绿色',
        color: '#6DCC51',
        router: '4-5',
        backgroundStatus: false,
      },
      {
        title: '蓝色',
        color: '#4BB8F3',
        router: '4-6',
        backgroundStatus: false,
      },
      {
        title: '紫色',
        color: '#D088E1',
        router: '4-7',
        backgroundStatus: false,
      },
      {
        title: '灰色',
        color: '#A5A5A8',
        router: '4-8',
        backgroundStatus: false,
      },
    ],
  },
  {
    title: '回收站',
    router: '5',
    icon: 'jeicon-trash-o',
    children: null,
  },
];

// 个人或者公司文件上面的文件
export const HEAD_MENU = [
  {
    title: '全部文件',
    icon: null,
    children: null,
    showEle: true,
  },
  {
    title: '文档',
    icon: null,
    showEle: false,
    children: [
      {
        title: 'Word',
        type: ['doc', 'docx', 'dot', 'dotx', 'docm'],
      },
      {
        title: 'Excel',
        type: ['xls', 'xlsx', 'xlsm'],
      },
      {
        title: 'PPT',
        type: ['pptx', 'pptm', 'ppt', 'potx', 'potm', 'pot', 'ppsx', 'ppsm', 'xml'],
      },
      {
        title: 'PDF',
        type: ['pdf'],
      },
    ],
  },
  {
    title: '图片',
    icon: null,
    showEle: false,
    children: [
      {
        title: 'JPG',
        type: ['jpg'],
      },
      {
        title: 'PNG',
        type: ['png'],
      },
      {
        title: 'GIF',
        type: ['gif'],
      },
      {
        title: 'SVG',
        type: ['svg'],
      },
      {
        title: 'JPEG',
        type: ['jpeg'],
      },
    ],
  },
  {
    title: '视频',
    icon: null,
    showEle: false,
    children: [
      {
        title: 'MV',
        type: ['rmvb', 'mv', 'avi', 'mtv', 'rm', '3gp', 'amv', 'flv', 'dmv', 'mp4', 'mpeg1', 'mpeg2', 'mpeg4'],
      },
      {
        title: 'Flash',
        type: ['swf', 'rm', 'mov', 'swf', 'fla'],
      },
    ],
  },
  {
    title: '音频',
    icon: null,
    showEle: false,
    children: [
      {
        title: 'MP3',
        type: ['mp3', 'wma', 'wav', 'ape', 'mid'],
      },
      {
        title: 'MP4',
        type: ['mp4', 'wma', 'wav', 'ape', 'mid'],
      },
    ],
  },
  {
    title: '压缩文件',
    icon: null,
    showEle: false,
    children: [
      {
        title: 'ZIP',
        type: ['zip', '7-zip', '7z', 'winzip'],
      },
      {
        title: 'RAR',
        type: ['rar', 'RAR'],
      },
    ],
  },
];

// export const FILE_HANDLE = [
//   {
//     text: '新建文件夹',
//     icon: 'jeicon-plus',
//     status: true,
//   },
//   {
//     text: '上传',
//     icon: 'jeicon-return-up',
//     status: true,
//   },
//   {
//     text: '下载',
//     icon: 'jeicon-return-down',
//     status: false,
//   },
//   {
//     text: '分享',
//     icon: 'jeicon-business-collaboration',
//     status: false,
//   },
//   {
//     text: '复制',
//     icon: 'jeicon-surface-copy',
//     status: false,
//   },
//   {
//     text: '移动到',
//     icon: 'jeicon-exchange',
//     status: false,
//   },
//   {
//     text: '至微邮',
//     icon: 'jeicon-envelope-o',
//     status: false,
//   },
//   {
//     text: '删除',
//     icon: 'jeicon-trash-o',
//     status: false,
//   },
// ];
