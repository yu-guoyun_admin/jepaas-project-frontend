/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-07 09:45:30
 * @LastEditors  : qinyonglian
 * @LastEditTime : 2020-01-09 18:29:10
 */
const HOST_BASIC = '';

// 这个文件到时候还需要修改，接口地址还需要添加， 部分接口也需要添加

// 恢复回收站
export const RECOVERY_RECYCLE_BIN = `${HOST_BASIC}/je/disk/recycle/recovery`;
// 文件预览
export const DOC_PREVIEW = `${HOST_BASIC}/je/disk/preview/text`;
// 清除回收站
export const EMPTY_RECYCLE_BIN = `${HOST_BASIC}/je/disk/recycle/clean`;
// 文件移动
export const FILE_MOVE = `${HOST_BASIC}/je/disk/opera/move`;
// 文件复制
export const FILE_COPY = `${HOST_BASIC}/je/disk/opera/copy`;
// 判断是否是文档管理员或者管理员
export const FILE_ADMIN = `${HOST_BASIC}/je/disk/isDocumentAdmin`;
// 网盘下载
export const SKYDRIVE_DOWNLOAD = `${HOST_BASIC}/je/disk/download`;
// 网盘下载(判断文件夹是否为空)
export const IS_SKYDRIVE_DOWNLOAD = `${HOST_BASIC}/je/disk/checkDownload`;
// 分享文件
export const SHARE_TRANSFER = `${HOST_BASIC}/je/disk/share`;
// 网盘上传
export const SKYDRIVE_UPLOAD = `${HOST_BASIC}/je/disk/file`;
// 目录搜索接口
export const FOLDER_QUERY = `${HOST_BASIC}/je/disk/folder/query`;
// 文件列表接口
export const FOLDER_LIST = `${HOST_BASIC}/je/disk/list`;
// 查询回收站接口
export const QUERY_RECYCLE_BIN = `${HOST_BASIC}/je/disk/recycle/list`;
// 发出分享列表
export const SEND_SHARE_LIST = `${HOST_BASIC}/je/disk/share/send/list`;
// 发出分享列表
export const RECIVE_SHARE_LIST = `${HOST_BASIC}/je/disk/share/receive/list`;
// 分享列表删除
export const SHARE_DELETE = `${HOST_BASIC}/je/disk/share/delete`;
// 分享列表的转存
export const SHARE_TARGET = `${HOST_BASIC}/je/disk/share/transfer`;
// 传输列表记录查询
export const TRANSFER_LIST_RECORD = `${HOST_BASIC}/je/disk/transfer/list`;
// 删除传输历史记录
export const DELETE_RECORD = `${HOST_BASIC}/je/disk/transfer/delete`;
// 将文件放入回收站
export const FILE_RECYCLE = `${HOST_BASIC}/je/disk/recycle`;
// 目录/文件重命名
export const FILE_RENAME = `${HOST_BASIC}/je/disk/rename`;
// 分享文件
export const SHAREING_FILES = `${HOST_BASIC}/je/disk/share`;
// 添加标签
export const ADD_TAGS = `${HOST_BASIC}/je/disk/tag`;
// 创建文件夹
export const CREATE_FOLDER = `${HOST_BASIC}/je/disk/folder/create`;
// 致微邮
export const MICRO_MAIL = `${HOST_BASIC}/je/disk/microMail`;
// 复制或者移动到的弹窗
export const LOAD_TREE = `${HOST_BASIC}/je/dd/tree/loadTree`;
// 设置公司文件的权限
export const SET_FILE = `${HOST_BASIC}/je/disk/empower`;
// 调取操作记录的接口
export const DISK_LOG = `${HOST_BASIC}/je/disk/log`;
// 获取权限人员
export const POWER_FILE = `${HOST_BASIC}/je/disk/powerNum`;
// 获取拖拽文件夹上传
export const UPLOAD_FILES = `${HOST_BASIC}/je/disk/takeStructure`;
