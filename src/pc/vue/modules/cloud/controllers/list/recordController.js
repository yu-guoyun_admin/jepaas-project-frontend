/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-11 15:39:57
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-11-22 17:04:08
 */

// 传输历史数据模型
import RecordModel from '../../model/recordModel';
import {
  shareListRecord,
  deleteRecords,
} from '../../actions/action';

export default class RecordList {
  /* 传输历史记录列表
   * @param {key} 搜索关键字
   * @param {order} 排序条件[{code:"time||size",type:"desc||asc"}]
   * @return:
   */
  async initList(key, order) {
    // 1.调取接口，渲染列表
    // 2.数据模型 {
    const res = await shareListRecord({
      key,
      order: JSON.stringify(order),
    });

    return res.map(item => RecordModel.create(item));
  }

  /* 删除历史记录数据
   * @param {transferIds} 删除记录的id
   * @return:
   */
  async deleteRecord(transferIds) {
    const res = await deleteRecords({
      transferIds: transferIds.join(','),
    });
    return res;
  }

  static create() {
    return new RecordList(...arguments);
  }
}
