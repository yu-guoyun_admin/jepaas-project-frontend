/**
 * ajax请求
 * @param {*} url
 * @param {*} params
 * @param {*} options
 */
export default function fetch(url, params, options = {}) {
  return new Promise((resolve, reject) => {
    params = options.data || params;
    JE.ajax({
      url,
      params,
      async: true,
      urlMaps: false,
      success(response) {
        let data;
        try {
          data = JE.getAjaxData(response, options.text);
        } catch (e) {
          // console.log('存在异常');
          data = response;
          data.responseHeader = data.getAllResponseHeaders();
        }
        resolve(data);
      },
      failure(response) {
        reject(response);
      },
    });
  });
}
